doclifter (2.21-3) unstable; urgency=medium

  * QA upload.
  * d/watch: Compatibility with new changes in GitLab.
    + Fix debian/watch no matching files for watch line.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Wed, 27 Mar 2024 00:11:39 -0300

doclifter (2.21-2) unstable; urgency=medium

  * QA upload.
  * d/copyright:
    - Add files tests/tapdiffer and tests/tapview with MIT-0.
    - Add files debian/* with contributors since 2002.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Sun, 18 Feb 2024 13:41:48 -0300

doclifter (2.21-1) unstable; urgency=medium

  * QA upload.

  [ Leandro Cunha ]
  * New upstream version.
  * d/patches: Refresh patches and fix FTBFS.
  * d/control: Bump Standards Version to 4.6.2.
  * d/upstream/metadata: Minor change.

  [ Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Fri, 16 Feb 2024 19:32:06 -0300

doclifter (2.20-2) unstable; urgency=medium

  * QA upload.
  * debian/patches/01-default-to-py3k.patch:
    - Fix information in header DEP 3 and Lintian report.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Sun, 26 Dec 2021 14:11:48 -0300

doclifter (2.20-1) unstable; urgency=medium

  * QA upload.

  [ Leandro Cunha ]
  * New upstream release.
  * Add debian/upstream/metadata.
  * Add debian/salsa-ci.yml.
  * debian/control:
    - Update debhelper-compat old 12 to 13.
    - Bump standards version to 4.6.0.1.
    - Add Rules-Requires-Root: no reported by Lintian.
  * debian/copyright:
    - Update years of maintenance to Eric S. Raymond <esr@thyrsus.com>.
    - Change source field.
  * debian/watch: change to Gitlab repository to import tarball.
  * debian/patches:
    - Drop 01-fix-makefile.patch applied by upstream.
    - Update default-to-py3k.patch and add header.
    - Rename default-to-py3k.patch to 01-default-to-py3k.patch.
  * debian/rules:
    - Fix FTBFS: not run tests.

  [ Debian Janitor ]
  * Update standards version to 4.4.1, no changes needed.

 -- Leandro Cunha <leandrocunha016@gmail.com>  Thu, 23 Dec 2021 19:02:31 -0300

doclifter (2.19-2) unstable; urgency=medium

  * QA upload.
  * Switch from python2 to python3; Closes: #936431

 -- Sandro Tosi <morph@debian.org>  Sun, 29 Sep 2019 18:20:34 -0400

doclifter (2.19-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * Upgrade to Standards-Version 4.4.0 (no changes).
  * Upgrade to debhelper compat level 12.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Sun, 25 Aug 2019 23:56:47 +0200

doclifter (2.18-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * Upgrade to Standards-Version 4.2.1 (no changes).
  * Mark doclifter as Multi-Arch: foreign (as recommended by the
    Multiarch hinter).
  * Update patches:
     - Remove patch 'profiler' (no longer applies).
     - Update patch 'manlifter-outdir' and rename it to
       00-manlifter-outdir.patch.
     - Rename patch 'fix-makefile' to 01-fix-makefile.patch.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Mon, 03 Sep 2018 12:17:08 +0200

doclifter (2.17-1) unstable; urgency=medium

  * QA upload.
  * Set Maintainer to Debian QA Group in debian/control (see #854220).
  * New upstream release (Closes: #739048).
  * Upgrade to debhelper compat level 11.
  * Remove Vcs-Bzr and Vcs-Browser fields from debian/control (the
    repository is no longer reachable).
  * Add new Vcs-Git and Vcs-Browser fields in debian/control.
  * Upgrade to Standards-Version 4.1.4 in debian/control (no changes).
  * Remove "Suggests: python-profiler" line from debian/control as
    python-profiler is no longer part of any Debian distribution.
  * Remove useless ">= 2.2" version restriction of python dependency.
  * Update patches.
  * Upgrade to version 4 format in debian/watch (no changes).
  * Upgrade debian/copyright to the machine-readable format.
  * Delete trailing whitespace from debian/changelog in order to
    silence the file-contains-trailing-whitespace Lintian tag.
  * Add xmlto as a build dependency in debian/control.
  * Mark python dependency with :any in debian/control.

 -- Fabian Wolff <fabi.wolff@arcor.de>  Mon, 14 May 2018 17:10:34 +0200

doclifter (2.11-1) unstable; urgency=low

  * New upstream version...
  * ...which fixes the .fi crashing problem.  Closes: #696576.
  * Disable the test suite if DEB_BUILD_OPTIONS=nocheck is set.
  * Uplift Standards-Version.

 -- Jeff Licquia <licquia@debian.org>  Sat, 08 Jun 2013 00:10:02 +0000

doclifter (2.7-1) unstable; urgency=low

  * New upstream version.
  * Update Standards-Version, fix new lintian issues, fix build
    warnings.
  * Add "make test" to build.

 -- Jeff Licquia <licquia@debian.org>  Sun, 18 Sep 2011 13:10:02 -0400

doclifter (2.6-1) unstable; urgency=low

  * New upstream version...
  * ...which fixes python2.6 issues differently.  Undo patch in NMU.
  * Switch to 3.0 (quilt) source format, undoing quilt integration
    in NMU.
  * Update Standards-Version and fix lintian warnings.
  * Tweak manlifter so it works without python-profiler.
  * Add -o option to manlifter, to match the man page.  Closes: #580243.
  * Update copyright to reflect GPL -> BSD switch.
  * Add Recommends for groff-base to get GNU eqn.

 -- Jeff Licquia <licquia@debian.org>  Sat, 19 Feb 2011 17:17:25 -0500

doclifter (2.3-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add patch for python2.6 issues. Closes: #580246.
  * Build-depend on quilt, and modify accordingly debian/rules.

 -- Serafeim Zanikolas <sez@debian.org>  Mon, 10 May 2010 19:59:31 +0200

doclifter (2.3-2) unstable; urgency=low

  * New maintainer.  Closes: #470885.
  * Include proper copyright notice in debian/copyright.
  * Update Standards-Version.
  * Include Homepage and Vcs-* headers.
  * Re-generate man pages.  Closes: #418643.
  * Fix other lintian warnings.
  * Recommend: plotutils (based on notes in the man page).

 -- Jeff Licquia <licquia@debian.org>  Tue, 27 May 2008 23:02:42 -0400

doclifter (2.3-1) unstable; urgency=low

  * New upstream release (closes: #405632)
  * debian/copyright: fix FSF address
  * Bump Standards version

 -- Ross Burton <ross@debian.org>  Thu, 11 Jan 2007 14:34:47 +0000

doclifter (2.1-1) unstable; urgency=low

  * New upstream release

 -- Ross Burton <ross@debian.org>  Sun, 23 Jan 2005 18:38:04 +0000

doclifter (1.8-1) unstable; urgency=low

  * New upstream release (closes: #245715)
  * Move to Standards 3.6.1.

 -- Ross Burton <ross@debian.org>  Mon, 26 Apr 2004 08:00:56 +0100

doclifter (1.0.4-1) unstable; urgency=low

  * New upstream release (closing: #190708)
  * debian/control: Updated email address
  * debian/copyright: Updated email address and web site URL.

 -- Ross Burton <ross@debian.org>  Fri, 25 Apr 2003 16:33:22 +0100

doclifter (1.0.3-1) unstable; urgency=low

  * New upstream release
  * Bump to Standards 3.5.8
  * Require debhelper >= 4.0.0.

 -- Ross Burton <ross@burtonini.com>  Wed,  5 Mar 2003 13:35:54 +0000

doclifter (1.0.0-3) unstable; urgency=low

  * Python 2.3 will probably be fine, so only require Python >= 2.2.

 -- Ross Burton <ross@burtonini.com>  Thu, 26 Sep 2002 14:56:50 +0100

doclifter (1.0.0-2) unstable; urgency=low

  * Increase Standards Version to 3.5.7.
  * Added a debian/watch file
  * doclifter: Directly execture /usr/bin/python instead of using env
    and python2.2
  * Change Depends to a versioned python, instead of python2.2

 -- Ross Burton <ross@burtonini.com>  Thu, 26 Sep 2002 13:09:19 +0100

doclifter (1.0.0-1) unstable; urgency=low

  * Initial Release.

 -- Ross Burton <ross@burtonini.com>  Thu, 22 Aug 2002 17:57:21 +0100
